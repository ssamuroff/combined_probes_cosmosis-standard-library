from cosmosis.datablock import names, option_section
import cPickle as pickle
import numpy as np
import string, pdb
from scipy import stats

# Many more redshift bin combinations than bins
# Get the lbin limits for each bin
# then apply the cuts.

def get_angular_frequency_cuts(block, samples, method_sh, method_pos, method_ggl, cut_per_bin, corr, chi_of_z, filename1, filename2, filename3):
	lmin_ee, lmax_ee = [],[]
	lmin_nn, lmax_nn = [],[]
	lmin_ne, lmax_ne = [],[]

	if filename1!=None:
		cuts_file1 = [np.loadtxt(filename1).T[1], np.loadtxt(filename1).T[2]]
	else:
		cuts_file1 = None

	if filename2!=None:
		cuts_file2 = [np.loadtxt(filename2).T[1], np.loadtxt(filename2).T[2]]
	else:
		cuts_file2 = None

	if filename3!=None:
		cuts_file3 = [np.loadtxt(filename3).T[1], np.loadtxt(filename3).T[2]]
	else:
		cuts_file3 = None

	if corr['ee']:
		nzbin = block["nz_"+samples['shear'], 'nbin']
		for i in xrange(nzbin):
			if cut_per_bin:
				lmin, lmax = choose_l_limits(block, cuts_file1, i+1, samples['shear'], method_sh, chi_of_z )
			else:
				lmin, lmax = choose_l_limits(block, cuts_file1, 1, samples['shear'], method_sh, chi_of_z)
			lmin_ee.append(lmin)
			lmax_ee.append(lmax)			

	if corr['nn']:
		nzbin = block["nz_"+samples['pos'], 'nbin']
		for i in xrange(nzbin):
			if cut_per_bin:
				lmin, lmax = choose_l_limits(block, cuts_file2, i+1, samples['pos'], method_pos, chi_of_z )
			else:
				lmin,lmax = choose_l_limits(block, cuts_file2, 1, samples['pos'], method_pos, chi_of_z )
			lmin_nn.append(lmin)
			lmax_nn.append(lmax)
	if corr['ne']:
		nzbin_1 = block["nz_"+samples['pos'], 'nbin']
		nzbin_2 = block["nz_"+samples['shear'], 'nbin']
		nzbin = max(nzbin_1, nzbin_2)
		for i in xrange(nzbin):
			if cut_per_bin:
				if i<nzbin_1:
					lmin, lmax = choose_l_limits(block, cuts_file3, i+1, samples['pos'], method_ggl, chi_of_z )
				else:
					lmin, lmax = 10., 3000.
			else:
				lmin,lmax = choose_l_limits(block, cuts_file3, 1, samples['pos'], method_ggl, chi_of_z )
			lmin_ne.append(lmin)
			lmax_ne.append(lmax)

	return lmin_ee, lmax_ee,lmin_nn, lmax_nn, lmin_ne, lmax_ne

def apply_angular_frequency_cuts(block, samples, corr, lmin_ee, lmax_ee, lmin_nn, lmax_nn, lmin_ne, lmax_ne):

	correlations=[]
	if corr['ee']:
		nzbin_shear = block["nz_"+samples['shear'], 'nbin']
		correlations += [(nzbin_shear,nzbin_shear, 'galaxy_shape_cl', lmin_ee, lmax_ee)]	
	if corr['nn']:
		nzbin_pos = block["nz_"+samples['pos'], 'nbin']
		correlations += [(nzbin_pos,nzbin_pos, 'galaxy_position_cl', lmin_nn, lmax_nn)]
	if corr['ne']:
		nzbin_pos = block["nz_"+samples['pos'], 'nbin']
		nzbin_shear = block["nz_"+samples['shear'], 'nbin']
		correlations += [(nzbin_pos,nzbin_shear, 'galaxy_position_shape_cross_cl', lmin_ne, lmax_ne)]

	for  sp in correlations:
			maxbin1, maxbin2 = sp[0],sp[1]
			l = block[sp[2], 'ell']
			lmin, lmax = sp[3], sp[4]
			block[sp[2], 'ell_min']= lmin[0]
			block[sp[2], 'ell_max']= lmax[0]
			for i in xrange(maxbin1):	
				for j in xrange(maxbin2):
					
					# Apply whichever cut is more stringent to this bin pairing
					# That will generally be the one from the lower redshift bin
					upper = min(lmax[i], lmax[j])
					lower =  max(lmin[i], lmin[j])
					print 'Applying scale cuts %s: %e > ell > %e on bin %d %d'%(sp[2], lower,upper,i+1,j+1)
					block[sp[2], 'lmin_%d_%d'%(i+1,j+1)]= lower
					block[sp[2], 'lmax_%d_%d'%(i+1,j+1)]= upper
					
def get_median_redshift(block, bin, cat):
	n_of_z = block["nz_"+cat, 'bin_%d'%bin] 
	z = block["nz_"+cat, 'z'] 
	prob_dist = n_of_z/n_of_z.sum()
	gen = stats.rv_discrete(values=(z,prob_dist), inc=z[1]-z[0])
	
	return gen.median()

def choose_l_limits(block, cuts_file, bin, sample, method, chi_of_z):
	if method=='fixed':
		return block[sample, 'lmin_%d'%bin], block[sample, 'lmax_%d'%bin]
	elif method=='rassat08':
		h = block['cosmological_parameters', 'h0']
		z_med = get_median_redshift(block, bin, sample)
		x = chi_of_z(z_med)

		kmax = 0.132 * z_med * h
		lmax = kmax * x
		return 10., 3.0*lmax
	elif method=='file':
		return cuts_file[0][bin-1], cuts_file[1][bin-1]
