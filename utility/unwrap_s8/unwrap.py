import numpy as np
from cosmosis.datablock import option_section, names as section_names

def setup(options):
	config = []
	return config

def execute(block, config):
	found={}
	calculated={}

	if block.has_value("cosmological_parameters", "s8"):
		S8 = block["cosmological_parameters", "s8"]
		found["s8"]=S8
	if block.has_value("cosmological_parameters", "sigma8_input") & block.has_value("cosmological_parameters", "s8"):
		sigma8 = block["cosmological_parameters", "sigma8_input"]
		found["sigma_8"] = sigma8

		omega_m = (S8/sigma8)**2
		calculated["omega_m"] = omega_m
		block.put_double("cosmological_parameters", "omega_m", omega_m)

	elif block.has_value("cosmological_parameters", "omega_m") & block.has_value("cosmological_parameters", "s8"):
		omega_m = block["cosmological_parameters", "omega_m"]
		sigma8 = S8/np.sqrt(omega_m)
		found["omega_m"] = omega_m
		calculated["sigma_8"] = sigma_8

	print "Read:"
	for name in found.keys():
		print "%s = %f"%(name, found[name])

	print "Derived:"
	for name in calculated.keys():
		print "%s = %f"%(name, calculated[name])

	return 0


def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
