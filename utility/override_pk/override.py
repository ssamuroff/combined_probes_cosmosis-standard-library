import numpy as np
from cosmosis.datablock import option_section, names as section_names

def setup(options):
	config = []
	return config

def execute(block, config):
	block["matter_power_nl", 'p_k'] = block["matter_power_lin", 'p_k']
	block["matter_power_nl", 'k_h'] = block["matter_power_lin", 'k_h']
	block["matter_power_nl", 'z'] = block["matter_power_lin", 'z']
	block["matter_power_nl", '_cosmosis_order_p_k'] = block["matter_power_lin", '_cosmosis_order_p_k']

	print "WARNING: assuming linear growth only."

	return 0


def cleanup(config):
	#nothing to do here!  We just include this 
	# for completeness
	return 0
