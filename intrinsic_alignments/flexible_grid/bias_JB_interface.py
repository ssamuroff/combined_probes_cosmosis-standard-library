from cosmosis.datablock import names, option_section
import bias_grid as functions

import numpy as np
import matplotlib.pyplot as plt
import pdb

def setup(options):
	nknodes = options.get_int(option_section, 'nknodes', default=5)
	nznodes = options.get_int(option_section, 'nznodes', default=8)
	ia = options.get_bool(option_section, 'intrinsic_alignments', default=True)
	gb = options.get_bool(option_section, 'galaxy_bias', default=False)
	zlim = options.get_double(option_section, 'zlim', default=4.0)
	opt= {'nznodes':nznodes, 'nknodes':nknodes, 'intrinsic_alignments': ia, 'galaxy_bias': gb}

	grid_generator = functions.flexible_grid(opt, zlim)
	return opt, grid_generator

def execute(block, config):

	options, grid = config

	# Define datablock section names
	nl= names.matter_power_nl
	cospar= names.cosmological_parameters

	# Use the grid object created during startup
	# with the specific realisation of the nodes
	grid.setup_grid_nodes(block)
	grid.interpolate_grid()
	grid.evaluate_and_save_bias(block)

	print 'Done.'
	return 0

def cleanup(config):
	pass
