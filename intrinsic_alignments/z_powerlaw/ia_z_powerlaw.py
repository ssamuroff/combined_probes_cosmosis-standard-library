#IA model from maccrann et al. 2015 - NLA with power law redshift scaling
#P_II(k,z) = ((1+z)^alpha) * F(z))^2 * P_delta(k,z), P_GI(k,z) = (1+z)^alpha * F(z) * P_delta(k,z)
#The P_II and P_GI before the redshift scaling are already in the block
#so just read them in and rescale

from cosmosis.datablock import names, option_section
import numpy as np

def setup(options):
	separate_eta = options.get_bool(option_section,"separate_GI_II", default=False)
	section = options.get_string(option_section,"output_section", default="")
	return separate_eta, section

def execute(block, config):

	do_separate_eta, section = config

	if section is not "":
		section="_"+section

	ia_section = names.intrinsic_alignment_parameters

	#read in power spectra
	z,k,p_ii=block.get_grid("intrinsic_power"+section,"z","k_h","P_k")
	z,k,p_gi=block.get_grid("matter_intrinsic_power"+section,"z","k_h","P_k")

	#read alpha from ia_section values section - JAZ this is a change from the internal one
	if do_separate_eta:
		print "Using separate scalings for II and GI"
		alpha_ii = block[ia_section,'alpha_ii%s'%section]
		alpha_gi = block[ia_section,'alpha_gi%s'%section]

	else:
		alpha_ii = block[ia_section,'alpha%s'%section]
		alpha_gi = block[ia_section,'alpha%s'%section]
	_,z_grid=np.meshgrid(k,z)

	#Construct and Apply redshift scaling
	z_scaling_ii=(1+z_grid)**alpha_ii
	z_scaling_gi=(1+z_grid)**alpha_gi
	p_ii*=z_scaling_ii**2
	p_gi*=z_scaling_gi

	#Save grid
	block.replace_grid("intrinsic_power%s"%section, "z", z, "k_h", k, "P_k", p_ii)
	block.replace_grid("matter_intrinsic_power%s"%section, "z", z, "k_h", k, "P_k", p_gi)
	return 0

def cleanup(config):
	pass