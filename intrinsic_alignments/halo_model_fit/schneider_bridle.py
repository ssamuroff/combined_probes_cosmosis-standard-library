import scipy.interpolate
import numpy as np

def choose_parameter_values(block, default_values, verbose):
	# Get the default parameter values
	if default_values is not None:
		if verbose:
			print "Using default parameters:"
			print default_values
		param = default_values

	# Or read them from the datablock
	else:
		dt= np.dtype([('index', '<f8'), ('spectrum', '<f8'), ('qi1', '<f8'), ('qi2', '<f8'), ('qi3', '<f8'), ('gamma_scale', '<f8')])
		param = np.zeros(6, dtype=dt)
		param["index"] = np.array([1,2,3,1,2,3])
		param["spectrum"] = np.array([0,0,0,1,1,1])

		for j, spectrum_name in zip([0,1], ["II", "GI"]):
			sel=(param["spectrum"]==j)
			for i in [1,2,3]:
				param[sel]["qi1"][i-1]=block["intrinsic_alignment_parameters", "q%d1_%s"%(i,spectrum_name)]
				param[sel]["qi2"][i-1]=block["intrinsic_alignment_parameters", "q%d2_%s"%(i,spectrum_name)]
				param[sel]["qi3"][i-1]=block["intrinsic_alignment_parameters", "q%d3_%s"%(i,spectrum_name)]
			param[sel]["gamma_scale"][i-1]=block["intrinsic_alignment_parameters", "gamma_scale_%s"%spectrum_name]

		if verbose:
			print "Found parameters:"
			print param

	return param 

def compute_1halo_spectrum(k, z, spectrum_type, parameters):
	#Select the half of the parameter table applicable for this correlation type
	param = parameters[(parameters["spectrum"]==spectrum_type)]
	pi=[]
	for j in [0,1,2]:
		pi.append( param["qi1"][j] * np.exp(param["qi2"][j] * (z**param["qi3"][j])) )

	p_k = np.zeros((len(z), len(k)))

	for i, ki in enumerate(k): 
		a = param["gamma_scale"][0] * (ki*ki/pi[0]/pi[0])
		if (spectrum_type==0):
			a*=a
		b = 1 + ((ki/pi[1])**pi[2])

		p_k.T[i] = a/b

	return p_k






