from cosmosis.datablock import names, option_section
import schneider_bridle as functions
import os
import numpy as np

# Based on the fitting function for the 1 halo contributions to the satellite
# spectra in Schneider and Bridle (2009) arXiv:0903.3870v1.pdf eq 19

def setup(options):
	suffix = options.get_string(option_section, "output_section", default="")
	use_defaults = options.get_bool(option_section, "use_default_parameters", default=True)
	verbose = options.get_bool(option_section, "verbose", default=True)

	if suffix:
		suffix= "_"+suffix

	if use_defaults:
		filepath = os.path.dirname(options.get_string(option_section, "file"))
		defaults=np.genfromtxt("%s/defaults.txt"%filepath,names=True)
	else:
		defaults=None
	
	return suffix, defaults, verbose

def execute(block, config):
	suffix, default_param, verbose = config

	nlpk = names.matter_power_nl
	ii = names.intrinsic_power + suffix
	gi = names.matter_intrinsic_power + suffix

	# We want the new spectra to be on the same grid in k,z as the nl P(k)
	# So load the relevant arrays
	k = block[nlpk, "k_h"]
	z = block[nlpk, "z"]

	# As specified either read in the default parameter values or look for 
	# them in the datablock
	param = functions.choose_parameter_values(block, default_param, verbose)

	p_ii = functions.compute_1halo_spectrum(k, z, 0, param)
	p_gi = functions.compute_1halo_spectrum(k, z, 1, param)

	block.put_grid(ii, "z", z, "k_h", k,  "p_k", p_ii)
	block.put_grid(gi, "z", z, "k_h", k,  "p_k", p_gi)

	return 0

def cleanup(config):
	pass