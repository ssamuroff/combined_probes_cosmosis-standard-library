#coding: utf-8
import scipy.interpolate
import numpy as np
import pdb
"""
This module generates the effective power P_12 
linear alignment KRHB model, which goes into C_ell
(under the Limber approximation) as :

C_ell = \int X_1(chi) X_2(chi) / chi^2 P_12(k=ell/chi,chi) d chi


"""


def compute_c1_baseline():
	C1_M_sun = 5e-14 # h^-2 M_S^-1 Mpc^3
	M_sun = 1.9891e30 # kg
	Mpc_in_m = 3.0857e22 # meters
	C1_SI = C1_M_sun / M_sun * (Mpc_in_m)**3  # h^-2 kg^-1 m^3
	#rho_crit_0 = 3 H^2 / 8 pi G
	G = 6.67384e-11 #m^3 kg^-1 s^-2
	H = 100 #Â h km s^-1 Mpc^-1
	H_SI = H * 1000.0 / Mpc_in_m  # h s^-1
	rho_crit_0 = 3 * H_SI**2 / (8*np.pi*G)  #  h^2 kg m^-3
	f = C1_SI * rho_crit_0
	return f

def resample_power(P1, P2, k1, k2):
	"Linearly resample P2 into the k values from P1. z values assumed equal"
	P_resampled = np.zeros_like(P1)
	nz = P1.shape[0]
	for i in xrange(nz):
		p_i = np.interp(np.log(k1), np.log(k2), np.log(abs(P2[i])))
		p_i_signed = np.interp(np.log(k1), np.log(k2), P2[i])

		P_resampled[i] = np.exp(p_i) * np.sign(p_i_signed)

	return P_resampled


#in units of rho_crit0
C1_RHOCRIT = compute_c1_baseline()
print "C1_RHOCRIT = ", C1_RHOCRIT


def bridle_king(z_nl, k_nl, P_nl, A, Omega_m):
	#extrapolate our linear power out to high redshift
	z0 = np.where(z_nl==0)[0][0]
	nz = len(z_nl)

	# P_II is actually fixed across redshifts
	# Which forces b_I to vary
	f = - A * Omega_m * C1_RHOCRIT

	# intrinsic-intrinsic term
	P_II = np.zeros_like(P_nl)
	for i in xrange(nz):
		P_II[i] = f**2 * P_nl[z0]

	growth = np.zeros_like(z_nl)
	ksmall = np.argmin(k_nl)
	P_GI = np.zeros_like(P_nl)
	for i in xrange(nz):
		growth = (P_nl[i,ksmall] / P_nl[z0,ksmall])**0.5
		P_GI[i] = f * P_nl[i] / growth

	P_II_resample = resample_power(P_nl, P_II, k_nl, k_lin)
	P_GI_resample = resample_power(P_nl, P_GI, k_nl, k_lin)

	# Finally calculate the intrinsic and stochastic bias terms from the power spectra
	b_I = -1.0 * np.sqrt(R1) * np.sign(A)
	r_I = P_GI/P_II * b_I

	return P_II, P_GI, b_I, r_I, k_nl

def bridle_king_corrected(z_nl, k_nl, P_nl, A, Omega_m):
	# What was used in CFHTLens and Maccrann et al.
	#extrapolate our linear power out to high redshift
	z0 = np.where(z_nl==0)[0][0]
	nz = len(z_nl)

	ksmall = np.argmin(k_nl)
	
	growth = (P_nl[:,ksmall] / P_nl[z0,ksmall])**0.5

	F = - A * C1_RHOCRIT * Omega_m / growth

	# intrinsic-intrinsic term
	P_II = np.zeros_like(P_nl)

	for i in xrange(nz):
		P_II[i,:] = F[i]**2 * P_nl[i,:] 

	P_GI = np.zeros_like(P_nl)
	for i in xrange(nz):
		P_GI[i] = F[i] * P_nl[i]


	# Finally calculate the intrinsic and stochastic bias terms from the power spectra
	R1 = P_II/P_nl
	b_I = -1.0 * np.sqrt(R1) * np.sign(A)
	r_I = P_GI/P_II * b_I

	return P_II, P_GI, b_I, r_I, k_nl


def kirk_rassat_host_bridle_power(z_lin, k_lin, P_lin, z_nl, k_nl, P_nl, A, Omega_m):
	""" 
	The Kirk, Rassat, Host, Bridle (2011) Linear Alignment model.
	Equations 8 and 9.

	C1 and rho_m0 must be in consistent units.
	The output P_II and P_GI will be specified at the same (k,z) as the linear power inputs

	The input f = A*C_1*rho_crit0

	"""

	#extrapolate our linear power out to high redshift
	z0 = np.where(z_lin==0)[0][0]
	nz = len(z_lin)

	# P_II is actually fixed across redshifts
	f = - Omega_m * A * C1_RHOCRIT

	# intrinsic-intrinsic term
	P_II = np.zeros_like(P_lin)
	for i in xrange(nz):
		P_II[i] = f**2 * P_lin[z0]

	# Make sure the k sampling of the two spectra are contiguous
	ikstart = np.argwhere((k_lin>k_nl[0]) & (k_lin<k_nl[-1])).T[0,0]
	ikend = np.argwhere((k_lin>k_nl[0]) & (k_lin<k_nl[-1])).T[0,-1]
	
	P_II = P_II[:, ikstart:ikend]
	P_lin = P_lin[:, ikstart:ikend] 			 
	k_lin = k_lin[ikstart:ikend]
	

	P_nl_resample = resample_power(P_lin, P_nl, k_lin, k_nl)

	growth = np.zeros_like(P_lin)
	ksmall = np.argmin(k_lin)
	for i in xrange(nz):
		growth[i] = (P_lin[i,ksmall] / P_lin[z0,ksmall])**0.5

	P_GI = f * P_lin**0.5 * P_nl_resample**0.5 / growth

	# Finally calculate the b_I and stochastic terms from the power spectra
	if A!=0.:
	    P_II_resample = resample_power(P_nl, P_II, k_nl, k_lin)
	    P_GI_resample = resample_power(P_nl, P_GI, k_nl, k_lin)
	    R1 = P_II_resample/P_nl
	    b_I = -1.0 * np.sqrt(R1) * np.sign(A)
	    r_I = P_GI_resample/P_II_resample * b_I
	else:
	    b_I = np.zeros_like(P_nl)
	    r_I = np.zeros_like(P_nl)
	    P_II_resample = np.zeros_like(P_nl)
	    P_GI_resample = np.zeros_like(P_nl)

	

	return P_II_resample, P_GI_resample, b_I, r_I, k_nl

def nla_flexible(z_lin, k_lin, P_lin, z_nl, k_nl, P_nl, A_GI, A_II, Omega_m):
	""" 
	A more flexible adaptation of the Kirk, Rassat, Host, Bridle (2011) Linear Alignment model.
	Equations 8 and 9.

	C1 and rho_m0 must be in consistent units.
	The output P_II and P_GI will be specified at the same (k,z) as the linear power inputs

	The input f = A*C_1*rho_crit0

	"""

	#extrapolate our linear power out to high redshift
	z0 = np.where(z_lin==0)[0][0]
	nz = len(z_lin)

	# P_II is actually fixed across redshifts
	f_GI = - Omega_m * A_GI * C1_RHOCRIT
	f_II = - Omega_m * A_II * C1_RHOCRIT

	# intrinsic-intrinsic term
	P_II = np.zeros_like(P_lin)
	for i in xrange(nz):
		P_II[i] = f_II * f_II * P_lin[z0]

	# Make sure the k sampling of the two spectra are contiguous
	ikstart = np.argwhere((k_lin>k_nl[0]) & (k_lin<k_nl[-1])).T[0,0]
	ikend = np.argwhere((k_lin>k_nl[0]) & (k_lin<k_nl[-1])).T[0,-1]
	
	P_II = P_II[:, ikstart:ikend]
	P_lin = P_lin[:, ikstart:ikend] 			 
	k_lin = k_lin[ikstart:ikend]
	

	P_nl_resample = resample_power(P_lin, P_nl, k_lin, k_nl)

	growth = np.zeros_like(P_lin)
	ksmall = np.argmin(k_lin)
	for i in xrange(nz):
		growth[i] = (P_lin[i,ksmall] / P_lin[z0,ksmall])**0.5

	P_GI = f_GI * P_lin**0.5 * P_nl_resample**0.5 / growth

	# Put the II and GI spectra on the same grid sampling in k,z as
	# the nonlinear matter power spectrum
	if A_II!=0.0:
		P_II_resample = resample_power(P_nl, P_II, k_nl, k_lin)
	else:
		P_II_resample = np.zeros_like(P_nl)
	if A_GI!=0.0:
		P_GI_resample = resample_power(P_nl, P_GI, k_nl, k_lin)
	else:
		P_GI_resample = np.zeros_like(P_nl)
	

	return P_II_resample, P_GI_resample, k_nl