from cosmosis.datablock import option_section, names
import pylab as plt
import scipy.stats.mstats as ms
import numpy as np

# This simple module is designed to stitch together the outputs of some combination
# of other IA codes to estimate the IA signal in a galaxy sample with an 
# arbitrary mixture of red and blue galaxies.
# It is assumed that power spectra for BOTH red and blue galaxies have been
# separately saved to the datablock prior to this module being called. 

allowed_redfraction_options=["file", "datablock"]

def setup(options):
	sec1 = options.get_string(option_section, "input_section_1", default="red")
	sec2 = options.get_string(option_section, "input_section_2", default="blue")
	galaxies = options.get_string(option_section, "galaxy_population", default="wl_number_density")
	f = red_fraction(options, galaxies)
	return sec1, sec2, f, galaxies

def execute(block, config):
	name1, name2, f_red, galaxies = config

	section_II = names.intrinsic_power
	section_GI = names.matter_intrinsic_power
	section_ia = names.intrinsic_alignment_parameters

	# Get relevant power spectra
	z,k,P_II_red = block.get_grid("%s_%s"%(section_II,name1),  "z", "k_h", "P_k")
	z,k,P_II_blue = block.get_grid("%s_%s"%(section_II,name2),  "z", "k_h", "P_k")
	P_II_red_blue = np.sqrt(P_II_red*P_II_blue)

	z,k,P_GI_red  = block.get_grid("%s_%s"%(section_GI,name1),  "z", "k_h", "P_k")
	z,k,P_GI_blue  = block.get_grid("%s_%s"%(section_GI,name2),  "z", "k_h", "P_k")


	# Source the red fraction as a fn of redshift for the galaxy sample in question
	f_red.get_fofz(block)
	f_red.fofz = np.array([list(f_red.fofz)]*k.size).T

	# Combine red, blue and cross terms
	P_II = (f_red.fofz * f_red.fofz) * P_II_red
	P_II += ((1-f_red.fofz) * (1-f_red.fofz)) * P_II_blue
	P_II += (f_red.fofz * (1-f_red.fofz)) * P_II_red_blue

	sign = (P_GI_red * P_GI_blue) / (abs(P_GI_red) * abs(P_GI_blue))

	P_GI = sign * np.exp((f_red.fofz) * np.log(np.abs(P_GI_red)) +  (1-f_red.fofz) * np.log(np.abs(P_GI_blue)))
	P_GI0 = (f_red.fofz) * P_GI_red +  (1-f_red.fofz) * P_GI_blue

	block.put_grid(section_II,  "z", z, "k_h", k, "P_k", P_II)
	block.put_grid(section_GI,  "z", z, "k_h", k, "P_k", P_GI)

	return 0

class red_fraction:
	def __init__(self, options, galaxies):
		"""Load basic choices and read in f(z) from file if specified."""
		self.source = options.get_string(option_section, "get_redfraction_from", default="datablock").lower()
		self.galaxies = options.get_string(option_section, "galaxy_population", default="wl_number_density")
		if self.source=="file":
			print "Reading a fixed red fraction as a function of redshift from disc."
			filename = options.get_string(option_section, "filename")
			self.z, self.fofz = np.loadtxt(filename).T
		else:
			print "No red fraction input found in setup phase."
			self.z, self.fofz = None,None

	def get_fofz(self, block):
		"""Obtain the red fraction as a function of redshift on the same 
		   z sampling as the matter power spectrum."""
		if (self.source=="file") and (self.fofz is not None):
			print "Have f(z) already. Nothing to be done."

		elif self.source=="datablock":
			self.read_from_block(block)

		self.interpolate(block)

	def read_from_block(self, block):
		"""Read a red fraction per redshift bin for a given galaxy population."""
		section = self.galaxies
		nbin = block.get_int("nz_%s"%section, "nbin")
		z = block["nz_%s"%section, "z"]
		self.fofz=[]
		self.z=[]
		for i in xrange(nbin):
			nz = block["nz_%s"%section, "bin_%d"%(i+1)]
			zmean = np.trapz(nz*z,z) / np.trapz(nz,z)
			self.z.append(zmean)
			self.fofz.append(block[section, "f_red_%d"%(i+1)])

	def interpolate(self, block):
		"""Interpolate the red fraction to the same redshift sampling as the 
		   matter power spectrum."""
		z = block["matter_power_nl", "z"]
		z0 = self.z
		fz = self.fofz
		self.fofz = np.interp(z,z0,fz)
		self.z = z

def check(k, p_red, p_blue, p):
	plt.plot(k,abs(p), color="purple", lw=2.5)
	plt.plot(k,abs(p_red), color="red",ls="--", lw=2.5)
	plt.plot(k,abs(p_blue), color="blue",ls="--", lw=2.5)
	plt.xscale("log")
	plt.yscale("log")
	plt.xlabel("$k$")
	plt.ylabel("$P(k)$")















