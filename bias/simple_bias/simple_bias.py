from cosmosis.datablock import option_section, names
import support 
import numpy as np
import pdb

#TODO
## TO INCLUDE CMB KAPPA
#- Add kappa as additional observable with 1 tomographic bin
#- Read and interpolate noise from Tommaso to each ell
#- Read 1 extra survey: area, ell bins
#- Add override option to use given area rather than the smallest survey

def setup(options):

	bias_per_bin = options.get_bool(option_section, 'bias_per_bin', default=False)
	gg = options.get_bool(option_section, 'position_position', default=False)
	mg = options.get_bool(option_section, 'magnification_position', default=False)
	gG = options.get_bool(option_section, 'position_shear', default=False)
	gI = options.get_bool(option_section, 'position_intrinsic', default=False)
	gk = options.get_bool(option_section, 'position_cmbkappa', default=False)

	verbosity = options.get_int(option_section, 'verbosity', default=1)

	config = gg, mg, gG, gI, gk, bias_per_bin, verbosity
	return config

def execute(block, config):
	gg, mg, gG, gI, gk, bias_per_bin, verbosity = config

	if gg:
		support.apply_galaxy_bias(block, verbosity, "galaxy_cl")
	if mg:
		support.apply_galaxy_bias(block, verbosity, "magnification_galaxy_cl")
	if gG:
		support.apply_galaxy_bias(block, verbosity, "galaxy_shear_cl")
	if gI:
		support.apply_galaxy_bias(block, verbosity, "galaxy_intrinsic_cl")
	if gk:
		support.apply_galaxy_bias(block, verbosity, "galaxy_cmbkappa_cl")

	return 0

def cleanup(config):
	return 0
