import numpy as np
from scipy import interpolate
import pdb 

def evaluate_mean_z(block, sample, bin):
	"""Integrate n(z) in each bin to get the mean redshift. """
	z_mean = []
	n_z = block[sample, "bin_%d"%(i+1)] 
	z = block[sample, "z"]
	for n in n_z:
		z_mean += [ sum(n*z)/sum(n) ]

	z_mean = np.array(z_mean)	

	return z_mean


def apply_galaxy_bias(block, verbosity, name):
	if name=="galaxy_cl":
		nbin_a = nbin_b = block[name, "nbin"]
		auto = True
	else:
		nbin_a = block[name, "nbin_a"]
		nbin_b = block[name, "nbin_b"]
		auto = False

	if name=="magnification_galaxy_cl":
		nbin = nbin_b
		mag = True
	else:
		nbin = nbin_a
		mag = False

	if verbosity>0:
		print "Correlation type: %s"%name

	for i in xrange(nbin_a):
		if auto:
			i0 = i
		else:
			i0 = 0
		for j in xrange(i0, nbin_b):
			if auto:
				cl = block[name, "bin_%d_%d"%(j+1, i+1)]
			else:
				cl = block[name, "bin_%d_%d"%(i+1, j+1)]
			if mag:
				bias = block["bias_parameters", "b_%d"%(j+1)]
			elif auto:
				bias = block["bias_parameters", "b_%d"%(i+1)] * block["bias_parameters", "b_%d"%(j+1)] 
			else:
				bias = block["bias_parameters", "b_%d"%(i+1)] 

			cl *= bias

			if auto:
				block[name, "bin_%d_%d"%(j+1, i+1)] = cl
			else:
				block[name, "bin_%d_%d"%(i+1, j+1)] = cl
			if verbosity>1:
				print "Scaling bin %d %d by bias coefficient %3.2e"%(i+1,j+1,bias)





