from cosmosis.datablock import option_section, names
from scipy.interpolate import interp1d
import math
import sstools.cosmosis_tools as tools
import numpy as np

MODES = ["skew", "mean", "width"]

def setup(options):
    additive_bias = options[option_section, "mean"]
    broadening = options[option_section, "width"]
    skew  = options[option_section, "tail"]
    per_bin = options[option_section, "bias_per_bin"]
    sample = options.get_string(option_section, "sample", default=None)
    rescale_depth = options.get_double(option_section, "rescale_depth", default=0.0)
    extend_maximum_redshift = options.get_bool(option_section, "extend_maximum_redshift", default=True)
    correct_mean = options.get_bool(option_section, "correct_mean", default=False)

    unmarginalised_bias = tools.get_double_or_array(options, "unmarginalised_bias", section=option_section, default=0.0)
   # except:
   #     unmarginalised_bias = options.get_double(option_section, "unmarginalised_bias", default=0.0)

    if (not additive_bias) and (not broadening) and (not skew):
        raise ValueError("please set one or more of: %r to T"%MODES)

    try: 
        cat_opt = options.get_string(option_section, "catastrophic_outliers")
    except: cat_opt = None

    if rescale_depth:
        print "WARNING: artificially rescaling n(z) to zmax=%3.3f"%rescale_depth
    
    return {"correct_mean":correct_mean, "additive":additive_bias, "broadening":broadening, "skew": skew, "sample": sample, "per_bin":per_bin, "catastrophic_outliers":cat_opt, "rescale":rescale_depth, "unmarginalised_bias":unmarginalised_bias, "deepen":extend_maximum_redshift }

def execute(block, config):
    additive, broadening, skew = config['additive'], config['broadening'], config['skew']
    pz = "nz_"+config['sample']
    biases = config['sample']
    nbin = block[pz, "nbin"]
    z = block[pz, "z"]

    fixed_bias = config["unmarginalised_bias"]

    if config["rescale"]:
        rescale_depth = config["rescale"]
        z = np.linspace(z.min(), rescale_depth, len(z))
        print "WARNING: artificially rescaling n(z) to zmax=%3.3f"%rescale_depth


    # Pad the nz with zeros to prevent an unphysical cutoff
    # if the distribution is shifted upwards in redshift
    z_med = z[int(len(z)/1.5)]
    d = z[1]-z[0]
    add_pts = int((z[-1] - z_med) / d)

    padz = np.arange(z.max()+d, z.max()+ (add_pts+1)*d, d )
    pad = np.zeros_like(padz)
    if config["deepen"]:
        z = np.append(z, padz)
    for i in xrange(1,nbin+1):
        bin_name = "bin_%d" % i
        nz = block[pz, bin_name]
        if config["deepen"]:
            nz = np.append(nz,pad)
        if config['per_bin']:
            if additive: 
                bias =block[biases, "bias_%d"%i]
                if not (np.atleast_1d(fixed_bias)==0.0).all():
                    print "WARNING: Applying a fixed bias %f"%(np.atleast_1d(fixed_bias)[i-1])
                    bias+=np.atleast_1d(fixed_bias)[i-1]
            if broadening:
                S = block[biases, "S_z_%d"%i]
            if skew:
                T = block[biases, "T_z_%d"%i]
        else:
            if additive:
                bias = block[biases, "bias_1"]
                if not (np.atleast_1d(fixed_bias)==0.0).all():
                    print "WARNING: Applying a fixed bias %f"%(np.atleast_1d(fixed_bias)[0])
                    bias+=np.atleast_1d(fixed_bias)[0]
            if broadening:
                S = block[biases, "S_z_1"]
            if skew:
                T =block[biases, "T_z_1"]
        dz = np.zeros_like(z)
        f = interp1d(z, nz, kind='cubic', fill_value = 0.0, bounds_error=False)
        #if mode=="multiplicative":
        #    nz_biased = f(z*(1-bias))
        if broadening:
            # Use the main peak of n(z) as a pivot point about which to distort n(z)
            zp = z[np.argwhere(nz==nz.max())[0][0]]
            dz += S*(z-zp)
        if additive and not broadening:
            dz -= bias
        nz_biased = f(z+dz)

        if skew:
            # Redistribute proability upwards in redshift witout altering
            # the peak of the n(z) 
            delta1 = T*(z-zp)+nz.max()
            delta2 = -1.*T*(z-zp)+nz.max()
            nz_biased += delta1 - delta2
            np.putmask(nz_biased, nz_biased<0., 0.)

        #normalise
        nz_biased/=np.trapz(nz_biased,z)

        # Add a population of catastrophic outliers
        # See Hearin et al (2010)

        if config['catastrophic_outliers']!=None:
            cat_mode = block[pz, 'method']
            fcat = block[pz,'fcat'] #0.05
            dzcat = block[pz,'dzcat']  #0.129
            zcat0= block[pz,'zcat0']  #0.65
            zcat = block[pz,'zcat']  #0.5
            sigcat= block[pz,'sigcat']  #0.1

            step = (dzcat/2.)-abs(z-zcat0)
            step[step==0.0] = -1.0
            step = 0.5*(step/abs(step)+1.0)

            dz = (z[1]-z[0])

            # Define a Gaussian island of outliers, normalised to 
            # the probability scattered from the affected region
            if cat_mode=='island':
                pcat = (1./(2.0*np.pi)**0.5 / sigcat) * np.exp(-1.0*(z-zcat)*(z-zcat) / (2.*sigcat*sigcat))
                pcat *= np.trapz(step*fcat*nz_biased,z)
                nz_biased = (1.-step*fcat)*nz_biased + pcat

            # Or scatter it uniformly across the theory redshift range
            elif cat_mode=='uniform':
                nz_biased = (1.-step*fcat)*nz_biased + step*fcat/(z[-1]-z[0])
        
        #renormalise
        np.putmask(nz_biased, nz_biased<0.0, 0.0)
        nz_biased/=np.trapz(nz_biased,z)
        if config["correct_mean"]:
            nz_biased = correct_mean(nz_biased, nz, z)

        # Reapply the shift in the mean if also varying deltaz
        if additive:
            dz = bias
            f = interp1d(z, nz_biased, kind='cubic', fill_value = 0.0, bounds_error=False)
            nz_biased = f(z+dz)
            nz_biased/=np.trapz(nz_biased,z)

        block[pz, bin_name] = nz_biased

    zmean = np.trapz(z*nz_biased, z) / np.trapz(nz_biased,z)
    print "Mean redshift in uppermost bin = %f"%zmean

    block[pz, 'z'] = z
    block[pz, "nz"] = len(z)


    return 0

def correct_mean(nz_biased, nz, z):
    #calculate the mean of each distribution and a shift between them
    zmean_biased = np.trapz(nz_biased*z)/np.trapz(nz_biased)
    zmean_unbiased = np.trapz(nz*z)/np.trapz(nz)
    zcorrection = zmean_unbiased - zmean_biased

    # Interpolate onto a fine redshift sampling
    zf=np.linspace(0, z.max(), 1000)
    nzfine=np.interp(zf, z, nz_biased)
    resolution = (zf[1:]-zf[:-1])[0]
    shift_required = zcorrection/resolution
    
    # Pad with zeros so there are no weird effects from nonzero end points
    # shifted from one end of the array to the other
    nzfine=[0.0]*100+list(nzfine)+[0.0]*100
    nzfineb=np.roll(np.array(nzfine), int(math.floor(0.5+shift_required)))
    nzfineb=nzfineb[100:-100]
    nz_corrected=np.interp(z,zf,nzfineb)

    return nz_corrected

def cleanup(config):
    pass
